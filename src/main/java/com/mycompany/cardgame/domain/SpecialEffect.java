/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cardgame.domain;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author potapov
 */
@Entity
@Table(name = "special_effect")
public class SpecialEffect implements Serializable {

	public enum ActivationTime {

		ON_DEATH, ON_APPEARENCE, EVERY_TURN, ON_HEAL, ON_TAKE_DAMAGE, ON_ACTIVATION;
	}

	public enum EffectType {

		DAMAGE(-1), HEAL(1), BUFF(0);
		private final int sign;

		private EffectType(int sign) {
			this.sign = sign;
		}

		public int getSign() {
			return sign;
		}
	}

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "description", nullable = false)
	private String description;

	@Column(name = "activation_time", nullable = false, unique = false)
	@Enumerated(EnumType.STRING)
	private ActivationTime activationTime;

	@Column(name = "power", nullable = false)
	private int power;

	@Column(name = "method", nullable = false) //TODO: use e-num
	private String method;

	@OneToMany(targetEntity = Card.class, cascade = CascadeType.ALL)
	private Set cards;

	public SpecialEffect() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

}
