/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cardgame.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author potapov
 */
@Entity
@Table(name = "card")
public class Card implements Serializable {

	public enum Type implements Serializable {

		BEAST, SPELL, BUFF;
	}

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "descr", nullable = false)
	private String descr;

	@Column(name = "cost", nullable = false)
	private int cost;

	@Column(name = "type", nullable = false, unique = false)
	@Enumerated(EnumType.STRING)
	private Type type;

	@Column(name = "card_face", nullable = false)
	private String img = "img/card/card-default.png";

	@Column(name = "card_face_prev", nullable = false)
	private String prev = "img/card/card-default-prew.png";

	@Column(name = "health", nullable = false)
	private int health;

	@Column(name = "damage", nullable = false)
	private int damage;

	@ManyToOne(targetEntity = Deck.class, optional = false)
	@JoinColumn(name = "deck_id")
	private Deck deck;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public Deck getDeck() {
		return deck;
	}

	public void setDeck(Deck deck) {
		this.deck = deck;
	}

	public Card() {
	}

	public Type getType() {
		return type;
	}

	public void setType(String type) {
		this.type = Type.valueOf(type);
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getPrev() {
		return prev;
	}

	public void setPrev(String prev) {
		this.prev = prev;
	}

}
