/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cardgame.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Master-Krab
 */
@Entity
@Table(name = "users")
public class User implements Serializable {

	public enum Role {

		USER, ADMIN
	}

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "user_role", length = 255, nullable = false)
	@Enumerated(EnumType.STRING)
	private Role role = Role.USER;

	@Column(name = "login", length = 255, nullable = false, unique = true)
	private String login;

	@Column(name = "password", length = 255, nullable = false)
	private String password;

	@Column(name = "name", length = 255)
	private String name;

	@Column(name = "avatar_url", length = 255)
	private String avatarUrl = "img/ava-2.png";

	@Column(name = "e-mail", length = 255)
	private String email;

	@Column(name = "rank")
	private Long rank = 1000L;

	@Column(name = "level")
	private Integer level = 1;

	@Column(name = "created_at")
	@Temporal(value = TemporalType.DATE)
	private LocalDate createdAt;

	public User() {
	}

	public User(Long id, String login, String password, String name, LocalDate creatingDate) {
		this.id = id;
		this.login = login;
		this.password = password;
		this.name = name;
		this.createdAt = creatingDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public LocalDate getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDate createdAt) {
		this.createdAt = createdAt;
	}

	public Long getRank() {
		return rank;
	}

	public void setRank(Long rank) {
		this.rank = rank;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
}
