/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.cardgame.domain;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author potapov
 */
@Entity
@Table(name = "speciality")
public class Speciality implements Serializable {
    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "name", nullable = false)
    private String name;
    
    @Column(name = "description")
    private String description;
    
    @OneToMany(targetEntity = Card.class, mappedBy = "speciality", cascade = CascadeType.ALL)
    private Set cards;
    
    @OneToMany(targetEntity = User.class, mappedBy = "speciality", cascade = CascadeType.ALL)
    private Set users;

	public Speciality() {
	}
	
}
