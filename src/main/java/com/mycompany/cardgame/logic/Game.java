/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cardgame.logic;

import com.mycompany.cardgame.logic.cards.Beast;
import com.mycompany.cardgame.domain.Card;
import com.mycompany.cardgame.domain.Deck;
import com.mycompany.cardgame.domain.User;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Master-Krab
 */
public class Game {

	public static final int MAX_PLAYER_COUNT = 2;
	public static final int FIRST_TURN = 1;

	public static class GameBuilder {

		private int playersHealth = 25;
		private final Map<User, Deck> users = new HashMap<>(MAX_PLAYER_COUNT);

		public void setPlayersHealth(int playersHealth) {
			this.playersHealth = playersHealth;
		}

		public void addUser(User user, Deck deck) {
			if (users.size() > 1) {
				throw new RuntimeException("Количество пользователей в игре не должно превышать 2");
			}

			users.put(user, deck);
		}

		public Game build() {
			if (users.size() < 2) {
				throw new RuntimeException("Количество пользователей меньше 2");
			}
			Game game = new Game();

			users.entrySet().stream().forEach((e) -> {
				List<Long> deck = e.getValue().getCards().stream()
						.map(Card::getId)
						.collect(Collectors.toList());
				Collections.shuffle(deck);
				game.decks.add(deck);
				game.players.add(new Player(e.getKey(), playersHealth));
			});

			game.currentPlayer = new SecureRandom().nextInt(2);

			return game;
		}

	}

	public static GameBuilder builder() {
		return new GameBuilder();
	}

	private int currentTurn = FIRST_TURN;
	private int currentPlayer = 0;
	private final List<Player> players = new ArrayList<>(MAX_PLAYER_COUNT);
	private final List<List<Beast>> cards = new ArrayList<>(MAX_PLAYER_COUNT); // Карты на столе
	private final List<List<Long>> decks = new ArrayList<>(MAX_PLAYER_COUNT); // Карты в колоде рубашкой вверх
	private final List<List<Card>> hands = new ArrayList<>(MAX_PLAYER_COUNT); // Карты на руках

	private Game() {
	}

	public int getCurrentTurn() {
		return currentTurn;
	}

	public Player getCurrentPlayer() {
		return players.get(currentPlayer);
	}

	public List<Card> getCurrentPlayerHand() {
		return hands.get(currentPlayer);
	}

	public List<List<Beast>> getCards() {
		return cards;
	}

	public void increaseTurn() {
		currentTurn++;
	}

	public void changeCurrentPlayer() {
		currentPlayer = currentPlayer == 0 ? 1 : 0;
	}

}
