/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cardgame.logic;

import com.mycompany.cardgame.domain.User;

/**
 *
 * @author Master-Krab
 */
public class Player implements IHasHealth {
	
	private int health;
	private final User user;

	Player(User u, int playersHealth) {
		health = playersHealth;
		user = u;
	}

	public int getHealth() {
		return health;
	}

	public User getUser() {
		return user;
	}

	@Override
	public void heal(int value) {
		health += value;
	}

	@Override
	public void takeDamage(int value) {
		health -= value;
	}

}
