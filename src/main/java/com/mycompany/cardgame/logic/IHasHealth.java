/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cardgame.logic;

/**
 *
 * @author Master-Krab
 */
public interface IHasHealth {

	void heal(int value);

	void takeDamage(int value);
}
