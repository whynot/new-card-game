/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cardgame.logic.cards;

/**
 *
 * @author Master-Krab
 */
public class Buff {

	public static final int DURATION_ONE_TURN = 1;
	public static final int DURATION_ENDLESS = 9999;

	public enum Type {

		DAMAGE, HEALTH, ARMOR, NONE;
	}

	private final int value;
	private final Type type;
	private final int duration;
	private int createdAt;

	public Buff(Type type, int value, int duration) {
		this.value = value;
		this.type = type;
		this.duration = duration;
	}

	public Buff(Type type, int value) {
		this.value = value;
		this.type = type;
		this.duration = DURATION_ONE_TURN;
	}

	public int getValue() {
		return value;
	}

	public Type getType() {
		return type;
	}

	public int getDuration() {
		return duration;
	}

	public void setCreatedAt(int turn) {
		createdAt = turn;
	}

	public boolean isActive(int turn) {
		return createdAt + duration >= turn;
	}
}
