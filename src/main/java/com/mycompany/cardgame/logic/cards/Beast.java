/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cardgame.logic.cards;

import com.mycompany.cardgame.logic.IHasHealth;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;

/**
 *
 * @author Master-Krab
 */
public class Beast implements IHasHealth {

	private final int health;
	private int currentHealth;
	private final int damage;
	private final int armor;

	private final List<Buff> activeBuffs = new ArrayList<>();

	public Beast(int health, int damage, int armor) {
		this.health = health;
		this.currentHealth = health;
		this.damage = damage;
		this.armor = armor;
	}

	public int getHealth() {
		OptionalInt bonus = activeBuffs.stream()
				.filter(b -> b.getType() == Buff.Type.HEALTH)
				.mapToInt(b -> b.getValue())
				.reduce((left, right) -> left + right);
		return health + bonus.orElse(0);
	}

	public int getCurrentHealth() {
		return currentHealth;
	}

	public int getDamage() {
		OptionalInt bonus = activeBuffs.stream()
				.filter(b -> b.getType() == Buff.Type.DAMAGE)
				.mapToInt(b -> b.getValue())
				.reduce((left, right) -> left + right);
		return damage + bonus.orElse(0);
	}

	public int getArmor() {
		OptionalInt bonus = activeBuffs.stream()
				.filter(b -> b.getType() == Buff.Type.ARMOR)
				.mapToInt(b -> b.getValue())
				.reduce((left, right) -> left + right);
		return armor + bonus.orElse(0);
	}

	public void addBuff(Buff buff) {
		activeBuffs.add(buff);
		if (buff.getType() == Buff.Type.HEALTH) {
			currentHealth += buff.getValue();
		}
	}

	public void removeAllBuffs() {
		activeBuffs.clear();
	}

	@Override
	public void heal(int value) {
		if ((currentHealth + value) > getHealth()) {
			currentHealth = getHealth();
		} else {
			currentHealth += value;
		}
	}

	@Override
	public void takeDamage(int value) {
		if (value > getArmor()) {
			currentHealth = currentHealth - (value - getArmor());
		}
	}
}
